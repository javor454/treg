create schema transaction;
create table transaction.transaction(
	id uuid default uuid_generate_v4() unique primary key,
	amount numeric not null,
	category text not null,
	timestamp timestamp default now()
);
