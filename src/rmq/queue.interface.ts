import { Options } from 'amqplib';

export interface QueueInterface {
  readonly name: string;
  readonly ops?: Options.AssertQueue;
}
