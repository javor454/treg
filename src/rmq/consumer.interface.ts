import { MessageInterface } from './message.interface';

export interface ConsumerInterface {
  consume(): Promise<void>;
  callback(message: MessageInterface, dependencies): Promise<boolean>;
}
