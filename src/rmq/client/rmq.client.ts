import { Injectable, Logger } from '@nestjs/common';
import { Connection, Channel, connect, ConsumeMessage, Replies } from 'amqplib';
import { ExchangeInterface } from '../exchange.interface';
import { QueueInterface } from '../queue.interface';
import { ExchangeQueueBond } from '../exchange-queue-bond';
import { MessageInterface } from '../message.interface';

@Injectable()
export class RmqClient {
  private connection: Connection | null = null;
  private channel: Channel | null = null;
  private logger: Logger = new Logger(RmqClient.name);

  async createExchange(exchange: ExchangeInterface): Promise<void> {
    const channel = await this.getChannel();

    this.logger.debug(`Asserting exchange "${exchange.name}".`);
    await channel.assertExchange(exchange.name, exchange.type, exchange.ops);
  }

  async createQueue(queue: QueueInterface): Promise<void> {
    const channel = await this.getChannel();

    this.logger.debug(`Asserting queue "${queue.name}".`);
    await channel.assertQueue(queue.name, queue.ops);
  }

  async bindQueue(bond: ExchangeQueueBond): Promise<void> {
    const channel = await this.getChannel();

    this.logger.debug(
      `Binding queue "${bond.getQueueName()}" to exchange "${bond.getExchangeName()}".`,
    );
    await channel.bindQueue(bond.getQueueName(), bond.getExchangeName(), bond.getBindingKey());
  }

  async produce(bond: ExchangeQueueBond, message: MessageInterface): Promise<boolean> {
    const channel = await this.getChannel();

    this.logger.debug(`Producing message: ${message.content.toString()}...`);
    return channel.publish(bond.getExchangeName(), bond.getBindingKey(), message.content);
  }

  async consume(
    queue: QueueInterface,
    callback: (msg: ConsumeMessage, deps) => Promise<boolean>,
    dependencies, // TODO: type?
  ): Promise<Replies.Consume> {
    const channel = await this.getChannel();

    await channel.prefetch(10); // TODO: enve

    this.logger.debug(`Consuming messages from queue: "${queue.name}".`);

    return channel.consume(queue.name, async (message: ConsumeMessage) => {
      this.logger.debug(`Consuming message: "${message.content.toString()}".`);
      const ack = await callback(message, dependencies);

      if (ack) {
        channel.ack(message);
      } else {
        channel.nack(message);
      }
    });
  }

  private async getChannel(): Promise<Channel> {
    if (this.connection === null) {
      this.logger.debug('Creating connection...');
      this.connection = await connect('amqp://dev:123@rabbitmq:5672'); // TODO: envs

      this.logger.debug('Creating channel...');
      this.channel = await this.connection.createChannel();
    }

    return this.channel;
  }

  async release(): Promise<void> {
    if (this.channel !== null) {
      this.logger.debug('Releasing channel...');
      await this.channel.close();
      this.channel = null;
    }

    if (this.connection !== null) {
      this.logger.debug('Releasing channel...');
      await this.connection.close();
      this.channel = null;
    }
  }
}
