import { Options } from 'amqplib';

export type ExchangeType = 'direct' | 'topic' | 'headers' | 'fanout' | 'match' | string;

export interface ExchangeInterface {
  readonly name: string;
  readonly type: ExchangeType;
  readonly ops?: Options.AssertExchange;
}
