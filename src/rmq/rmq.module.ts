import { Module, OnApplicationShutdown } from '@nestjs/common';
import { RmqClient } from './client/rmq.client';
import { ModuleRef } from '@nestjs/core';

@Module({
  providers: [RmqClient],
  exports: [RmqClient],
})
export class RmqModule implements OnApplicationShutdown {
  constructor(private readonly moduleRef: ModuleRef) {}

  onApplicationShutdown(signal?: string) {
    const client = this.moduleRef.get(RmqClient);
    return client.release();
  }
}
