import { QueueInterface } from './queue.interface';
import { ExchangeInterface } from './exchange.interface';

export class ExchangeQueueBond {
  constructor(
    private readonly queue: QueueInterface,
    private readonly exchange: ExchangeInterface,
    private readonly bindingKey: string,
  ) {}

  getQueueName(): string {
    return this.queue.name;
  }

  getExchangeName(): string {
    return this.exchange.name;
  }

  getBindingKey(): string {
    return this.bindingKey;
  }
}
