export class ConfigException extends Error {
  constructor(configName: string, reason: string) {
    super(`Config: "${configName}" validation failed: ${reason}.`);
  }
}
