import { Module, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { HealthModule } from './health/health.module';
import { TransactionModule } from './transaction/transaction.module';
import { StatementModule } from './statement/statement.module';
import { AppService } from './app.service';

@Module({
  imports: [HealthModule, TransactionModule, StatementModule],
  controllers: [],
  providers: [AppService],
})
export class AppModule implements OnApplicationBootstrap, OnApplicationShutdown {
  constructor(private readonly service: AppService) {}

  async onApplicationBootstrap(): Promise<void> {
    await this.service.setupWorkers(1);
  }

  async onApplicationShutdown(signal?: string): Promise<void> {
    await this.service.shutdownWorkers(signal);
  }
}
