import { isDefined, isNumberString, isString } from 'class-validator';
import { ConfigException } from '../config/config.exception';

export const ES_CONFIG = Symbol('ES_CONFIG_TOKEN');

export class EsConfig {
  readonly username: string;
  readonly password: string;
  readonly port: number;

  constructor(username?: string, password?: string, port?: string) {
    if (!isDefined(username) || !isString(username))
      throw new ConfigException('EsConfig', 'Elasticsearch username not defined or not string.');

    if (!isDefined(password) || !isString(password))
      throw new ConfigException('EsConfig', 'Elasticsearch password not defined or not string.');

    if (!isDefined(port) || !isNumberString(port))
      throw new ConfigException(
        'EsConfig',
        'Elasticsearch port not defined or not numeric string.',
      );

    this.username = username;
    this.password = password;
    this.port = Number(port);
  }
}

export const featureEsConfig = (): EsConfig =>
  new EsConfig(process.env.ES_USER, process.env.ES_PASS, process.env.ES_PORT);
