import { Injectable, Logger } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { AbstractDocument } from '../index/abstract-document';
import { IndexInterface } from '../index/index.interface';

// TODO: create dynamic module for this i guess
// TODO: rename to client
@Injectable()
export class BaseEsRepository {
  private readonly logger: Logger = new Logger(BaseEsRepository.name);

  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async ping(): Promise<boolean> {
    this.logger.debug('Pinging cluster...');
    return this.elasticsearchService.ping();
  }

  async verifyStructures(indices: IndexInterface[]): Promise<void> {
    this.logger.debug('Verifying structures...');
    for (const index of indices) {
      const exists = await this.elasticsearchService.indices.exists({
        index: index.name,
      });

      if (!exists) {
        await this.createMapping(index);
      }
    }
    this.logger.debug('Structures verified.');
  }

  async save(index: IndexInterface, doc: AbstractDocument): Promise<void> {
    this.logger.debug(`Saving document: ${doc.toJson()}.`);
    await this.elasticsearchService.index(
      {
        index: index.name,
        document: doc.toJson(),
      },
      {
        headers: {
          'content-type': 'application/json', // mandatory
        },
      },
    );

    await this.elasticsearchService.indices.refresh();
  }

  async search(index: IndexInterface, query: Record<string, any>) {
    this.logger.debug(`Searching index ${index.name} with query ${JSON.stringify(query)}`);

    return this.elasticsearchService.search({
      index: index.name,
      body: {
        query: {
          match: query,
        },
      },
    });
  }

  private async createMapping(index: IndexInterface): Promise<void> {
    this.logger.debug(`Query to create ${index.name} index...`);
    const ack = await this.elasticsearchService.indices.create({
      index: index.name,
      mappings: index.mapping,
    });
    /*    const ack = await this.elasticsearchService.indices.putMapping({
          index: index.name,
          properties: index.mapping,
        });*/
    console.log(ack);
  }
}
