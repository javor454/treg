import { MappingTypeMapping } from '@elastic/elasticsearch/lib/api/types';

export interface IndexInterface {
  readonly name: string;
  readonly type: string;
  readonly mapping: MappingTypeMapping;
}
