export abstract class AbstractDocument {
  toJson(): string {
    return JSON.stringify(this);
  }
}
