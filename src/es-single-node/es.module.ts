import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ES_CONFIG, EsConfig, featureEsConfig } from './es.config';
import { ElasticsearchModule } from '@nestjs/elasticsearch';
import { BaseEsRepository } from './repository/base-es-repository.service';

@Global()
@Module({
  imports: [
    ConfigModule.forFeature(featureEsConfig),
    ElasticsearchModule.register({
      node: getElasticNodeUrl(featureEsConfig()),
    }),
  ],
  providers: [
    BaseEsRepository,
    {
      provide: ES_CONFIG,
      useValue: featureEsConfig(),
    },
  ],
  exports: [ElasticsearchModule, BaseEsRepository],
})
export class EsModule {}

function getElasticNodeUrl(esConfig: EsConfig): string {
  return 'http://elastic:'.concat(esConfig.port.toString());
}
