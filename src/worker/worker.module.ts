import { Module, OnApplicationBootstrap } from '@nestjs/common';
import { StatementConsumer } from './statement/rmq/statement.consumer';
import { RmqModule } from '../rmq/rmq.module';
import { StatementService } from './statement/statement.service';
import { PgModule } from '../pg/pg.module';
import { TransactionRepository } from './transaction/pg/transaction.repository';
import { EsModule } from '../es-single-node/es.module';
import { StatisticsRepository } from './statistics/es/repository/statistics.repository';
import { StatisticsBuilder } from './statistics/builder/statistics.builder';

@Module({
  imports: [RmqModule, PgModule, EsModule],
  providers: [
    StatementConsumer,
    StatementService,
    {
      provide: 'TransactionRepository',
      useClass: TransactionRepository,
    },
    {
      provide: 'StatisticsRepository',
      useClass: StatisticsRepository,
    },
    StatisticsBuilder,
  ],
})
export class WorkerModule implements OnApplicationBootstrap {
  constructor(private readonly consumer: StatementConsumer) {}

  async onApplicationBootstrap(): Promise<void> {
    await this.consumer.consume();
  }
}
