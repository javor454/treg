import { StatisticsInterface } from './statistics.interface';

export class YearlyStatistics implements StatisticsInterface {
  constructor(readonly statementId: string, readonly yearlySum: number) {}

  toJson(): string {
    return JSON.stringify({ statementId: this.statementId, yearlySum: this.yearlySum.toString(2) });
  }
}
