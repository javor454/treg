import { StatisticsInterface } from './statistics.interface';

export class MonthlyStatistics implements StatisticsInterface {
  constructor(readonly statementId: string, readonly monthlySum: number) {}

  toJson(): string {
    return JSON.stringify({
      statementId: this.statementId,
      monthlySum: this.monthlySum.toFixed(2),
    });
  }
}
