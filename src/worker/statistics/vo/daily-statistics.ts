import { StatisticsInterface } from './statistics.interface';

export class DailyStatistics implements StatisticsInterface {
  constructor(readonly statementId: string, readonly dailySum: number) {}

  toJson(): string {
    return JSON.stringify({ statementId: this.statementId, dailySum: this.dailySum.toFixed(2) });
  }
}
