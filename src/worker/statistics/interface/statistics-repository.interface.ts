import { DailyStatistics } from '../vo/daily-statistics';
import { MonthlyStatistics } from '../vo/monthly-statistics';
import { YearlyStatistics } from '../vo/yearly-statistics';

export interface StatisticsRepositoryInterface {
  verifyStructures(): void;
  saveDailyStatistics(stats: DailyStatistics): void;
  saveMonthlyStatistics(stats: MonthlyStatistics): void;
  saveYearlyStatistics(stats: YearlyStatistics): void;
}
