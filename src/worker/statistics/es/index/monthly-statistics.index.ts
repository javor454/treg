import { IndexInterface } from '../../../../es-single-node/index/index.interface';
import { MappingTypeMapping } from '@elastic/elasticsearch/lib/api/types';

export class MonthlyStatisticsIndex implements IndexInterface {
  readonly name = 'monthly-statistics';
  readonly type = 'monthly-statistics';
  readonly mapping: MappingTypeMapping = {
    properties: {
      statementId: {
        type: 'text',
      },
      monthlySum: {
        type: 'scaled_float',
        scaling_factor: 100,
      },
    },
  };
}
