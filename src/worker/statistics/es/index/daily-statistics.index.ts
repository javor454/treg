import { IndexInterface } from '../../../../es-single-node/index/index.interface';
import { MappingTypeMapping } from '@elastic/elasticsearch/lib/api/types';

export class DailyStatisticsIndex implements IndexInterface {
  readonly name = 'daily-statistics';
  readonly type = 'daily-statistics';
  readonly mapping: MappingTypeMapping = {
    properties: {
      statementId: {
        type: 'text',
      },
      dailySum: {
        type: 'scaled_float',
        scaling_factor: 100,
      },
    },
  };
}
