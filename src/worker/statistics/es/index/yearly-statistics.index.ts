import { IndexInterface } from '../../../../es-single-node/index/index.interface';
import { MappingTypeMapping } from '@elastic/elasticsearch/lib/api/types';

export class YearlyStatisticsIndex implements IndexInterface {
  readonly name = 'yearly-statistics';
  readonly type = 'yearly-statistics';
  readonly mapping: MappingTypeMapping = {
    properties: {
      statementId: {
        type: 'text',
      },
      yearlySum: {
        type: 'scaled_float',
        scaling_factor: 100,
      },
    },
  };
}
