import { Injectable } from '@nestjs/common';
import { IndexInterface } from '../../../../es-single-node/index/index.interface';
import { BaseEsRepository } from '../../../../es-single-node/repository/base-es-repository.service';
import { StatisticsRepositoryInterface } from '../../interface/statistics-repository.interface';
import { YearlyStatistics } from '../../vo/yearly-statistics';
import { MonthlyStatistics } from '../../vo/monthly-statistics';
import { DailyStatistics } from '../../vo/daily-statistics';
import { DailyStatisticsIndex } from '../index/daily-statistics.index';
import { MonthlyStatisticsIndex } from '../index/monthly-statistics.index';
import { YearlyStatisticsIndex } from '../index/yearly-statistics.index';

/*
  List index info: http://localhost:9200/daily-statistics
  List documents: http://localhost:9200/daily-statistics/_search?size=10&q=*:*
*/
@Injectable()
export class StatisticsRepository implements StatisticsRepositoryInterface {
  protected readonly dailyIndex = new DailyStatisticsIndex();
  protected readonly monthlyIndex = new MonthlyStatisticsIndex();
  protected readonly yearlyIndex = new YearlyStatisticsIndex();

  constructor(private readonly esRepository: BaseEsRepository) {}

  async verifyStructures(): Promise<void> {
    await this.esRepository.verifyStructures(this.getIndices());
  }

  protected getIndices(): IndexInterface[] {
    return [this.dailyIndex, this.monthlyIndex, this.yearlyIndex];
  }

  async saveDailyStatistics(stats: DailyStatistics): Promise<void> {
    await this.esRepository.save(this.dailyIndex, stats);
  }

  async saveMonthlyStatistics(stats: MonthlyStatistics): Promise<void> {
    await this.esRepository.save(this.monthlyIndex, stats);
  }

  async saveYearlyStatistics(stats: YearlyStatistics): Promise<void> {
    await this.esRepository.save(this.yearlyIndex, stats);
  }
}
