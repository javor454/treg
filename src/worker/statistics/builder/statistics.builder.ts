import { Injectable } from '@nestjs/common';
import { TransactionInterface } from '../../transaction/pg/transaction.interface';
import { DailyStatistics } from '../vo/daily-statistics';
import { MonthlyStatistics } from '../vo/monthly-statistics';
import { YearlyStatistics } from '../vo/yearly-statistics';
import { CreateStatementDto } from '../../../statement/dto/create-statement.dto';

@Injectable()
export class StatisticsBuilder {
  async buildDailyStatistics(
    dto: CreateStatementDto,
    transactions: TransactionInterface[],
  ): Promise<DailyStatistics> {
    // TODO: improve algo
    let sum = 0;
    for (const transaction of transactions) {
      sum += transaction.amount;
    }

    return new DailyStatistics(dto.getId(), sum);
  }

  async buildMonthlyStatistics(
    dto: CreateStatementDto,
    transactions: TransactionInterface[],
  ): Promise<MonthlyStatistics> {
    let sum = 0;
    for (const transaction of transactions) {
      sum += transaction.amount;
    }

    return new MonthlyStatistics(dto.getId(), sum);
  }

  async buildYearlyStatistics(
    dto: CreateStatementDto,
    transactions: TransactionInterface[],
  ): Promise<YearlyStatistics> {
    let sum = 0;
    for (const transaction of transactions) {
      sum += transaction.amount;
    }

    return new YearlyStatistics(dto.getId(), sum);
  }
}
