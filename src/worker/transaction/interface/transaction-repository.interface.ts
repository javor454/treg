import { TransactionInterface } from '../pg/transaction.interface';

export interface TransactionRepositoryInterface {
  getTransactionsByDate(from: Date, to: Date): Promise<TransactionInterface[]>;
}
