import { CategoryEnum } from '../../../transaction/enum/category.enum';

export interface TransactionInterface {
  readonly id: number;
  readonly amount: number;
  readonly category: CategoryEnum;
  readonly timestamp: Date;
}
