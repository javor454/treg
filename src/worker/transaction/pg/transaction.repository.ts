import { Injectable } from '@nestjs/common';
import { PgClient } from '../../../pg/pg.client';
import { TransactionRepositoryInterface } from '../interface/transaction-repository.interface';
import { TransactionInterface } from './transaction.interface';
import { CategoryEnum } from '../../../transaction/enum/category.enum';

@Injectable()
export class TransactionRepository implements TransactionRepositoryInterface {
  constructor(private readonly client: PgClient) {}

  async getTransactionsByDate(from: Date, to: Date): Promise<TransactionInterface[]> {
    const res: { rows: TransactionInterface[] } = await this.client.query(
      'SELECT * FROM transaction.transaction tra WHERE tra.timestamp BETWEEN $1 AND $2',
      [from.toISOString(), to.toISOString()],
    );

    return res.rows.map((transaction) => {
      return {
        id: transaction.id,
        amount: Number(transaction.amount),
        category: transaction.category as CategoryEnum,
        timestamp: new Date(transaction.timestamp),
      };
    });
  }
}
