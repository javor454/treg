import { CreateStatementDto } from '../../statement/dto/create-statement.dto';
import { Inject, Injectable } from '@nestjs/common';
import { Type } from '../../statement/enum/type';
import * as moment from 'moment';
import { TransactionRepositoryInterface } from '../transaction/interface/transaction-repository.interface';
import { StatisticsBuilder } from '../statistics/builder/statistics.builder';
import { StatisticsRepositoryInterface } from '../statistics/interface/statistics-repository.interface';

@Injectable()
export class StatementService {
  constructor(
    @Inject('TransactionRepository')
    private readonly transactionRepository: TransactionRepositoryInterface,
    @Inject('StatisticsRepository')
    private readonly statisticsRepository: StatisticsRepositoryInterface,
    private readonly statisticsBuilder: StatisticsBuilder,
  ) {}

  async createStatement(dto: CreateStatementDto): Promise<void> {
    const { from, to } = await StatementService.extractDateInterval(dto.type, dto.date);
    const transactions = await this.transactionRepository.getTransactionsByDate(from, to);

    if (dto.type === Type.Daily) {
      const dailyStats = await this.statisticsBuilder.buildDailyStatistics(dto, transactions);
      await this.statisticsRepository.saveDailyStatistics(dailyStats);
    }
    if (dto.type === Type.Monthly) {
      const monthlyStats = await this.statisticsBuilder.buildMonthlyStatistics(dto, transactions);
      await this.statisticsRepository.saveMonthlyStatistics(monthlyStats);
    }
    if (dto.type === Type.Yearly) {
      const yearlyStats = await this.statisticsBuilder.buildYearlyStatistics(dto, transactions);
      await this.statisticsRepository.saveYearlyStatistics(yearlyStats);
    }

    if (dto.type === null) {
      throw new Error('Unknown statement type.'); // TODO: exception
    }
  }

  private static async extractDateInterval(
    type: Type,
    date: Date,
  ): Promise<{ from: Date; to: Date }> {
    if (type === Type.Daily) {
      return {
        from: moment(date).startOf('day').toDate(),
        to: moment(date).endOf('day').toDate(),
      };
    }

    if (type === Type.Monthly) {
      return {
        from: moment(date).startOf('month').toDate(),
        to: moment(date).endOf('month').toDate(),
      };
    }

    if (type === Type.Yearly) {
      return {
        from: moment(date).startOf('year').toDate(),
        to: moment(date).endOf('year').toDate(),
      };
    }
  }
}
