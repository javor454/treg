import { CreateStatementDto } from '../../../statement/dto/create-statement.dto';
import { Type } from '../../../statement/enum/type';
import { Format } from '../../../statement/enum/format';
import { Injectable } from '@nestjs/common';
import { StatementService } from '../statement.service';
import { ConsumerInterface } from '../../../rmq/consumer.interface';
import { RmqClient } from '../../../rmq/client/rmq.client';
import { CreateStatementQueue } from '../../../common/statement/rmq/queue/create-statement.queue';
import { MessageInterface } from '../../../rmq/message.interface';

@Injectable()
export class StatementConsumer implements ConsumerInterface {
  constructor(private readonly service: StatementService, private readonly rmqClient: RmqClient) {}

  async consume(): Promise<void> {
    await this.rmqClient.consume(CreateStatementQueue, this.callback, {
      service: this.service,
    });
  }

  async callback(
    message: MessageInterface,
    dependencies: { service: StatementService },
  ): Promise<boolean> {
    const content: { type: string; date: string; format: string; id: string } = JSON.parse(
      message.content.toString(), // TODO: yolo parse
    );

    const dto = new CreateStatementDto(
      content.type as Type,
      new Date(content.date),
      content.format as Format,
      content.id,
    );

    await dependencies.service.createStatement(dto);

    return true; // TODO: ?
  }
}
