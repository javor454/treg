export abstract class ValidationException extends Error {
  protected constructor(message: string) {
    super(message);
  }
}
