import { QueueInterface } from '../../../../rmq/queue.interface';

export const CreateStatementQueue: QueueInterface = {
  name: 'create-statement-queue',
  ops: {
    durable: true,
    maxLength: 1000, // TODO: env
  },
};
