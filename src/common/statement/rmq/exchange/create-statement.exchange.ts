import { ExchangeInterface } from '../../../../rmq/exchange.interface';

export const CreateStatementExchange: ExchangeInterface = {
  name: 'create-statement-exchange',
  type: 'direct',
  ops: {
    durable: true,
  },
};
