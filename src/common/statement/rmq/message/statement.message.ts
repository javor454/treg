import { MessageInterface } from '../../../../rmq/message.interface';
import { CreateStatementDto } from '../../../../statement/dto/create-statement.dto';

export class StatementMessage extends MessageInterface {
  static fromDto(dto: CreateStatementDto): StatementMessage {
    return new StatementMessage(
      Buffer.from(
        JSON.stringify({
          type: dto.getType(),
          date: dto.getDateString(),
          format: dto.getFormat(),
          id: dto.getId(),
        }),
      ),
    );
  }
}
