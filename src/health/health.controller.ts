import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { PgClient } from '../pg/pg.client';

@Controller()
@ApiTags('Utility')
export class HealthController {
  constructor(private readonly pgClient: PgClient) {}

  @Get('v1/health')
  async check(@Res() res: Response): Promise<void> {
    // TODO: use terminus
    let json = {
      status: 'ok',
      pg: 'up',
    };

    if ((await this.checkPgHealth()) === false) {
      json = {
        status: 'err',
        pg: 'down',
      };
    }
    res.status(HttpStatus.OK).json(json);
  }

  private async checkPgHealth(): Promise<boolean> {
    try {
      await this.pgClient.query('SELECT 1;');
    } catch (e) {
      return false;
    }

    return true;
  }
}
