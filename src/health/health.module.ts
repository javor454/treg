import { Module } from '@nestjs/common';
import { HealthController } from './health.controller';
import { PgModule } from '../pg/pg.module';

@Module({
  imports: [PgModule],
  controllers: [HealthController],
  providers: [],
})
export class HealthModule {}
