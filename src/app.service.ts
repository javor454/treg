import { Injectable, Logger } from '@nestjs/common';
import * as _cluster from 'cluster';
const cluster = _cluster as unknown as _cluster.Cluster; // typings hack

@Injectable()
export class AppService {
  private logger = new Logger(AppService.name);

  async setupWorkers(count: number): Promise<void> {
    // TODO: put in some object
    for (let i = 0; i < count; i++) {
      const worker = cluster.fork();

      worker.addListener('online', () => {
        this.logger.debug(`Worker with pid:${worker.process.pid} is online.`);
      });
      worker.addListener('disconnect', () => {
        this.logger.debug(`Worker with pid:${worker.process.pid} was disconected.`);
      });
      worker.addListener('error', () => {
        this.logger.debug(`Worker with pid:${worker.process.pid} caught error.`);
        // TODO: handle error?
      });
      worker.addListener('exit', () => {
        this.logger.debug(`Worker with pid:${worker.process.pid} died. Restarting...`);
        this.setupWorkers(1); // TODO: recursion error?
      });
    }
  }

  async shutdownWorkers(signal?: string): Promise<void> {
    this.logger.debug('Shutting down cluster' + (signal ? ` with signal: ${signal}` : '...'));
    cluster.disconnect();
  }
}
