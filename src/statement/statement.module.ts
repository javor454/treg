import { Module, OnApplicationBootstrap } from '@nestjs/common';
import { PgModule } from '../pg/pg.module';
import { StatementHandler } from './statement.handler';
import { StatementController } from './http/statement.controller';
import { StatementRunner } from './cli/statement.runner';
import { RmqModule } from '../rmq/rmq.module';
import { StatementProducer } from './rmq/statement.producer';
import { TransactionModule } from '../transaction/transaction.module';
import { StatementRepository } from './rmq/statement.repository';
import { EsModule } from '../es-single-node/es.module';

@Module({
  imports: [PgModule, RmqModule, TransactionModule, EsModule],
  controllers: [StatementController],
  providers: [
    StatementHandler,
    StatementRunner,
    StatementProducer,
    {
      provide: 'StatementRepository',
      useClass: StatementRepository,
    },
  ],
})
export class StatementModule implements OnApplicationBootstrap {
  constructor(private readonly producer: StatementProducer) {}

  async onApplicationBootstrap(): Promise<void> {
    await this.producer.initEnvironment();
  }
}
