import { Type } from '../enum/type';
import { Format } from '../enum/format';
import { IsDate, IsDefined, IsString } from 'class-validator';
import { v4 } from 'uuid';

export class CreateStatementDto {
  @IsDefined({ message: 'Field type is not defined.' })
  @IsString({ message: 'Field type is not a string.' })
  readonly type: Type;

  @IsDefined({ message: 'Field date is not defined.' })
  @IsDate({ message: 'Field date is not a valid date.' })
  readonly date: Date;

  @IsDefined({ message: 'Field format is not defined.' })
  @IsString({ message: 'Field format is not a string.' })
  readonly format: Format;

  readonly id: string;

  constructor(type: Type, date: Date, format: Format, id?: string) {
    this.type = type;
    this.date = date;
    this.format = format;
    this.id = id ? id : v4();
  }

  getType(): string {
    return this.type;
  }

  getDateString(): string {
    return this.date.toISOString();
  }

  getFormat(): string {
    return this.format;
  }

  getId(): string {
    return this.id;
  }
}
