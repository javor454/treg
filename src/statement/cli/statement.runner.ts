import { ConsoleLogger, Injectable } from '@nestjs/common';
import { Command, CommandRunner } from 'nest-commander';
import { StatementHandler } from '../statement.handler';
import { CreateStatementDtoFactory } from '../factory/create-statement-dto.factory';

@Command({
  name: 'statement',
  arguments: '<type> <date> <format>',
  description: 'Create statement with type, date, format.',
})
@Injectable()
export class StatementRunner implements CommandRunner {
  private readonly logger = new ConsoleLogger('CLI:'.concat(StatementRunner.name));

  constructor(private readonly handler: StatementHandler) {}

  async run(
    passedParams: string[],
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    options?: Record<string, any>,
  ): Promise<void> {
    const [type, date, format] = passedParams;
    this.logger.log(
      `Request to queue create statement job with type: "${type}", date: "${date}, format: "${format}."`,
    );

    try {
      await this.createStatement(type, date, format);
    } catch (e) {
      this.logger.error(e);
    }
  }

  async createStatement(type: string, date: string, format: string): Promise<void> {
    // TODO: void?
    const dto = await CreateStatementDtoFactory.create(type, date, format);
    await this.handler.handleCreate(dto);

    this.logger.log('Statement job succesfully queued via CLI.');
  }
}
