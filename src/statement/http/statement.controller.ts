import {
  Controller,
  Get,
  HttpStatus,
  Logger,
  Param,
  Post,
  Res,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Response } from 'express';
import { ApiBadRequestResponse, ApiTags } from '@nestjs/swagger';
import { StatementHandler } from '../statement.handler';
import { Format } from '../enum/format';
import { Type } from '../enum/type';
import { CreateStatementDtoFactory } from '../factory/create-statement-dto.factory';

@Controller()
@ApiTags('Retrieve statement')
@UsePipes(new ValidationPipe())
export class StatementController {
  private readonly logger = new Logger('HTTP:'.concat(StatementController.name));

  constructor(private readonly handler: StatementHandler) {}

  @Post('v1/statement/daily/:date')
  @ApiBadRequestResponse({
    description: '<p>Invalid date input: "test".</p>',
  })
  async getDaily(@Param('date') date: string, @Res() response: Response): Promise<void> {
    const type = Type.Daily;
    const format = Format.Json; // TODO: parse header with format to accept
    this.logger.log(
      `Request to queue create statement job with type: "${type}", date: "${date}", format: "${format}".`,
    );
    const id = await this.handler.handleCreate(
      await CreateStatementDtoFactory.create(type, date, Format.Json),
    );

    this.logger.log(`Statement job with id:${id} succesfully queued via HTTP.`);
    response.status(HttpStatus.CREATED).send({
      'statement-uuid': id,
    });
  }

  @Post('v1/statement/monthly/:date')
  @ApiBadRequestResponse({
    description: '<p>Invalid date input: "test".</p>',
  })
  async getMonthly(@Param('date') date: string, @Res() response: Response): Promise<void> {
    const type = Type.Monthly;
    const format = Format.Json; // TODO: parse header with format to accept
    this.logger.log(
      `Request to queue create statement job with type: "${type}", date: "${date}", format: "${format}".`,
    );
    const id = await this.handler.handleCreate(
      await CreateStatementDtoFactory.create(type, date, format),
    );

    this.logger.log(`Statement job with id:${id} succesfully queued via HTTP.`);
    response.status(HttpStatus.CREATED).send({
      'statement-uuid': id,
    });
  }

  @Post('v1/statement/yearly/:date')
  @ApiBadRequestResponse({
    description: '<p>Invalid date input: "test".</p>',
  })
  async getYearly(@Param('date') date: string, @Res() response: Response): Promise<void> {
    const type = Type.Yearly;
    const format = Format.Json; // TODO: parse header with format to accept
    this.logger.log(
      `Request to queue create statement job with type: "${type}", date: "${date}", format: "${format}".`,
    );
    const id = await this.handler.handleCreate(
      await CreateStatementDtoFactory.create(type, date, format),
    );

    this.logger.log(`Statement job with id:${id} succesfully queued via HTTP.`);
    response.status(HttpStatus.CREATED).send({
      'statement-uuid': id,
    });
  }

  @Get('v1/statement/:id')
  async getStatement(@Param('id') id: string, @Res() response: Response): Promise<void> {
    this.logger.log(`Request to get statement with id: "${id}".`);
    let statement;
    try {
      statement = await this.handler.handleGet(id);
    } catch (e) {
      response.status(HttpStatus.NOT_FOUND).send({
        message: e.message,
      });
    }

    response.status(HttpStatus.OK).send({
      statement: statement,
    });
  }
}
