import { RmqClient } from '../../rmq/client/rmq.client';
import { Injectable } from '@nestjs/common';
import { ExchangeQueueBond } from '../../rmq/exchange-queue-bond';
import { StatementMessage } from '../../common/statement/rmq/message/statement.message';
import { CreateStatementExchange } from '../../common/statement/rmq/exchange/create-statement.exchange';
import { CreateStatementQueue } from '../../common/statement/rmq/queue/create-statement.queue';

@Injectable()
export class StatementProducer {
  private readonly createStatementExchange = CreateStatementExchange;

  private readonly createStatementQueue = CreateStatementQueue;

  private readonly bond = new ExchangeQueueBond(
    this.createStatementQueue,
    this.createStatementExchange,
    'create-statement',
  );

  constructor(private readonly rmqClient: RmqClient) {}

  async initEnvironment(): Promise<void> {
    // TODO: move to separate service
    await this.rmqClient.createExchange(this.createStatementExchange);
    await this.rmqClient.createQueue(this.createStatementQueue);
    await this.rmqClient.bindQueue(this.bond);
  }

  async produce(message: StatementMessage): Promise<void> {
    await this.rmqClient.produce(this.bond, message);
  }
}
