import { StatementRepositoryInterface } from '../interface/statement-repository.interface';
import { Injectable } from '@nestjs/common';
import { BaseEsRepository } from '../../es-single-node/repository/base-es-repository.service';
import { DailyStatisticsIndex } from '../../worker/statistics/es/index/daily-statistics.index';
import { MonthlyStatisticsIndex } from '../../worker/statistics/es/index/monthly-statistics.index';
import { YearlyStatisticsIndex } from '../../worker/statistics/es/index/yearly-statistics.index';

@Injectable()
export class StatementRepository implements StatementRepositoryInterface {
  private readonly dailyIndex = new DailyStatisticsIndex();
  private readonly monthlyIndex = new MonthlyStatisticsIndex();
  private readonly yearlyIndex = new YearlyStatisticsIndex();

  constructor(private readonly esRepository: BaseEsRepository) {}

  async getStatement(id: string) {
    let res = await this.esRepository.search(this.dailyIndex, {
      statementId: id,
    });

    if (res.hits.hits.length !== 0) {
      return res.hits.hits[0]._source;
    }

    res = await this.esRepository.search(this.monthlyIndex, {
      statementId: id,
    });

    if (res.hits.hits.length !== 0) {
      return res.hits.hits[0]._source;
    }

    res = await this.esRepository.search(this.yearlyIndex, {
      statementId: id,
    });

    if (res.hits.hits.length !== 0) {
      return res.hits.hits[0]._source;
    }

    throw new Error('Statement not found.');
  }
}
