import { ValidationException } from '../../common/exception/validation.exception';
import { ValidationError } from 'class-validator';

// TODO: duplicated functionality
export class CreateStatementDtoValidationException extends ValidationException {
  constructor(errors: ValidationError[]) {
    let message = '';
    errors.forEach((error) => {
      message = message + error.toString();
    });

    super(message);
  }
}
