import { CreateStatementDto } from '../dto/create-statement.dto';
import { Type } from '../enum/type';
import { Format } from '../enum/format';
import { validate } from 'class-validator';
import { CreateStatementDtoValidationException } from '../exception/invalid-date.exception';

export class CreateStatementDtoFactory {
  static async create(type: string, date: string, format): Promise<CreateStatementDto> {
    const dto = new CreateStatementDto(type as Type, new Date(date), format as Format);
    const errors = await validate(dto);

    if (errors.length >= 1) {
      throw new CreateStatementDtoValidationException(errors);
    }

    return dto;
  }
}
