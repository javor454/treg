export enum Type {
  Daily = 'daily',
  Monthly = 'monthly',
  Yearly = 'yearly',
}
