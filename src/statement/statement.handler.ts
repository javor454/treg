import { Inject, Injectable } from '@nestjs/common';
import { CreateStatementDto } from './dto/create-statement.dto';
import { StatementProducer } from './rmq/statement.producer';
import { StatementMessage } from '../common/statement/rmq/message/statement.message';
import { StatementRepositoryInterface } from './interface/statement-repository.interface';

@Injectable()
export class StatementHandler {
  constructor(
    private readonly producer: StatementProducer,
    @Inject('StatementRepository')
    private readonly repository: StatementRepositoryInterface,
  ) {}

  async handleCreate(dto: CreateStatementDto): Promise<string> {
    await this.producer.produce(StatementMessage.fromDto(dto));

    return dto.getId();
  }

  async handleGet(id: string) {
    // TODO: type
    return this.repository.getStatement(id);
  }
}
