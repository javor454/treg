import { Injectable, Logger } from '@nestjs/common';
import { Command, CommandRunner } from 'nest-commander';
import { PgMigrationExecutor } from '../pg-migration.executor';

@Command({
  name: 'run',
  description: 'Run postgres migrations.',
})
@Injectable()
export class MigrationRunner implements CommandRunner {
  private readonly logger = new Logger('CLI:'.concat(MigrationRunner.name));

  constructor(private readonly executor: PgMigrationExecutor) {}

  async run(passedParams: string[], options?: Record<string, any>): Promise<void> {
    this.logger.log('Request to execute postgres migrations.');

    try {
      await this.executor.execute();
    } catch (e) {
      this.logger.error(`Unhandled error: ${e}.`);
    }
  }
}
