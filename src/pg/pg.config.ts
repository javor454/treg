import { isDefined, IsNumberString, isString } from 'class-validator';
import { ConfigException } from '../config/config.exception';

export const PG_CONFIG = Symbol('PG_CONFIG_TOKEN');

export class PgConfig {
  readonly port: number | undefined;
  readonly host = 'postgres';

  constructor(
    readonly user: string | undefined,
    readonly database: string | undefined,
    readonly password: string | undefined,
    port?: string,
  ) {
    const stringFields = new Map([
      ['Postgres user', user],
      ['Postgres db', database],
      ['Postgres password', password],
    ]);

    stringFields.forEach((name, field) => {
      PgConfig.assertStringField(name, field);
    });

    if (!isDefined(port) || !IsNumberString(port))
      throw new ConfigException(
        'PgConfig',
        `Postgres port not defined or not numeric string.`,
      );

    this.port = Number(port);
  }

  private static assertStringField(name: string, value: any): void {
    if (!isDefined(value) || !isString(value)) {
      throw new ConfigException(
        'PgConfig',
        `${name} not defined or not string.`,
      );
    }
  }
}

export const featurePgConfig = (): PgConfig =>
  new PgConfig(
    process.env.PG_USER,
    process.env.PG_DB,
    process.env.PG_PASS,
    process.env.PG_PORT,
  );
