import { Inject, Injectable, Logger } from '@nestjs/common';
import { PG_POOL } from './pg-pool.factory';
import { Pool } from 'pg';
import { loadMigrationFiles, migrate } from 'postgres-migrations';

const MIGRATIONS_DIRECTORY = process.cwd().concat('/migration');

@Injectable()
export class PgMigrationExecutor {
  private readonly logger = new Logger(PgMigrationExecutor.name);

  constructor(@Inject(PG_POOL) private pool: Pool) {}

  async execute(): Promise<void> {
    const client = await this.pool.connect();

    try {
      this.logger.log('Executing migrations...');
      await migrate({ client }, MIGRATIONS_DIRECTORY);
      this.logger.log('Migrations executed successfully.');
    } catch (e) {
      this.logger.error(e);
    } finally {
      await client.release();
    }
  }

  async areMigrationFilesValid(): Promise<boolean> {
    // TODO: log migrations ok
    // TODO: is this necessary tho
    // TODO: validate migrations with `pg-validate-migrations "path/to/migration/files".` -> move to CI?
    return (await loadMigrationFiles(MIGRATIONS_DIRECTORY)) !== undefined;
  }
}
