import { Inject, Injectable, Logger } from '@nestjs/common';
import { Pool, QueryResult } from 'pg';
import { PG_POOL } from './pg-pool.factory';
import { PgException } from './exception/pg.exception';

@Injectable()
export class PgClient {
  private readonly logger = new Logger(PgClient.name);

  constructor(@Inject(PG_POOL) private pool: Pool) {}

  async query(query: string, values: any[] = []): Promise<QueryResult> {
    return this.pool
      .query(query, values)
      .then((res: QueryResult) => {
        this.logger.debug(
          `Query executed: "${query}"` + (values.length >= 1 ? ` with values: "${values}."` : '.'),
        );
        return res;
      })
      .catch((e) => {
        this.logger.error(e);
        throw new PgException(e);
      });
  }
}
