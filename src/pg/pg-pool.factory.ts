import { PgConfig } from './pg.config';
import { Pool } from 'pg';

export const PG_POOL = Symbol('PG_POOL');

export const pgPoolFactory = async (config: PgConfig) => {
  // TODO: log new pool
  return new Pool(config);
};
