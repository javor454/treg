export class PgException extends Error {
  constructor(e) {
    super(`Pg error: ${e}.`);
  }
}
