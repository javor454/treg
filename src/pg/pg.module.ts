import { Logger, Module, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { featurePgConfig, PG_CONFIG } from './pg.config';
import { PgClient } from './pg.client';
import { PG_POOL, pgPoolFactory } from './pg-pool.factory';
import { ModuleRef } from '@nestjs/core';
import { Pool } from 'pg';
import { PgMigrationExecutor } from './pg-migration.executor';
import { MigrationRunner } from './cli/migration.runner';

@Module({
  imports: [ConfigModule.forFeature(featurePgConfig)],
  providers: [
    PgClient,
    PgMigrationExecutor,
    {
      provide: PG_CONFIG,
      useValue: featurePgConfig(),
    },
    {
      provide: PG_POOL,
      inject: [PG_CONFIG],
      useFactory: pgPoolFactory,
    },
    MigrationRunner,
  ],
  exports: [PgClient],
})
export class PgModule implements OnApplicationBootstrap, OnApplicationShutdown {
  private readonly logger: Logger = new Logger('PgModule');

  constructor(
    private readonly moduleRef: ModuleRef,
    private readonly migrations: PgMigrationExecutor,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    // TODO: setup only for local and run only once
    await this.migrations.execute();
  }

  onApplicationShutdown(signal?: string) {
    this.logger.debug('Releasing PG pool.');
    const pool = this.moduleRef.get(PG_POOL) as Pool;
    return pool.end();
  }
}
