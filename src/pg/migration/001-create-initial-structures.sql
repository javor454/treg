create schema transaction;
create table transaction.transaction(
	id serial primary key,
	amount numeric not null,
	category text not null
);