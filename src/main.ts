import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger, ValidationPipe } from '@nestjs/common';
import * as _cluster from 'cluster';
const cluster = _cluster as unknown as _cluster.Cluster; // typings hack
import { WorkerModule } from './worker/worker.module';

const logger = new Logger('Main');

async function bootstrapMaster(): Promise<void> {
  logger.log('Bootstraping master...');
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('api');
  app.enableShutdownHooks();
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true, // transform payload to custom defined objects
    }),
  );

  const config = new DocumentBuilder()
    .setTitle('Transaction register API documentation')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/docs', app, document);

  await app.listen(3000);
}

async function bootstrapWorker(): Promise<void> {
  logger.log('Bootstraping worker...');
  const app = await NestFactory.create(WorkerModule);

  app.enableShutdownHooks();

  await app.init();
}

const bootstrap = cluster.isPrimary ? bootstrapMaster : bootstrapWorker;

bootstrap().catch((e) => {
  console.error(e);
  process.exit(1);
});
