import { CategoryEnum } from '../enum/category.enum';

export class Transaction {
  constructor(
    readonly amount: number,
    readonly category: CategoryEnum,
    readonly timestamp: Date,
    readonly uuid: string,
  ) {}
}
