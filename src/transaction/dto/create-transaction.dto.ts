import { ApiProperty } from '@nestjs/swagger';
import { CategoryEnum } from '../enum/category.enum';
import { IsDefined, IsEnum, IsNumber } from 'class-validator';
import { v4 as uuidv4 } from 'uuid';

export class CreateTransactionDto {
  @IsDefined({ message: 'Field amount is not defined.' })
  @IsNumber(undefined, { message: 'Field amount is not a number.' })
  @ApiProperty({
    default: 123.45,
  })
  readonly amount: number;

  @IsDefined({ message: 'Field category is not defined.' })
  @IsEnum(CategoryEnum, { message: 'Field category is not a valid Category.' })
  @ApiProperty({
    enum: CategoryEnum,
  })
  readonly category: CategoryEnum;

  readonly timestamp = new Date();

  readonly uuid: string;

  constructor(amount: number, category: CategoryEnum) {
    this.amount = amount;
    this.category = category;
    this.timestamp = new Date();
    this.uuid = uuidv4();
  }
}
