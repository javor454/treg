import { CreateTransactionDto } from '../dto/create-transaction.dto';
import { Transaction } from '../entity/transaction';
import { Injectable } from '@nestjs/common';

@Injectable()
export class TransactionFactory {
  async createFromDto(createDto: CreateTransactionDto): Promise<Transaction> {
    return new Transaction(
      createDto.amount,
      createDto.category,
      createDto.timestamp,
      createDto.uuid,
    );
  }
}
