import { CreateTransactionDto } from '../dto/create-transaction.dto';
import { validate } from 'class-validator';
import { CategoryEnum } from '../enum/category.enum';
import { CreateTransactionDtoValidationException } from '../exception/create-transaction-dto-validation.exception';

export class CreateTransactionDtoFactory {
  static async create(amount: string, category: string): Promise<CreateTransactionDto> {
    const dto = new CreateTransactionDto(Number(amount), category as CategoryEnum);
    const errors = await validate(dto);

    if (errors.length >= 1) {
      throw new CreateTransactionDtoValidationException(errors);
    }
    return dto;
  }
}
