import { Module } from '@nestjs/common';
import { TransactionController } from './http/transaction.controller';
import { TransactionHandler } from './transaction.handler';
import { TransactionRepository as PgTransactionRepository } from './pg/transaction.repository';
import { PgModule } from '../pg/pg.module';
import { CreateTransactionDtoFactory } from './factory/create-transaction-dto.factory';
import { TransactionRunner } from './cli/transaction.runner';
import { TransactionFactory } from './factory/transaction.factory';

@Module({
  imports: [PgModule],
  controllers: [TransactionController],
  providers: [
    TransactionHandler,
    {
      provide: 'PgTransactionRepository',
      useClass: PgTransactionRepository,
    },
    TransactionRunner,
    CreateTransactionDtoFactory,
    TransactionFactory,
  ],
  exports: [
    {
      provide: 'PgTransactionRepository',
      useClass: PgTransactionRepository,
    },
  ],
})
export class TransactionModule {}
