import { Transaction } from '../entity/transaction';

export interface TransactionRepositoryInterface {
  create(transaction: Transaction): void;
}
