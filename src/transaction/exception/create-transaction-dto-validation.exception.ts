import { ValidationException } from '../../common/exception/validation.exception';
import { ValidationError } from 'class-validator';

export class CreateTransactionDtoValidationException extends ValidationException {
  constructor(errors: ValidationError[]) {
    let message = '';
    errors.forEach((error) => {
      message = message + error.toString();
    });

    super(message);
  }
}
