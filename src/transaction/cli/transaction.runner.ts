import { TransactionHandler } from '../transaction.handler';
import { CreateTransactionDtoFactory } from '../factory/create-transaction-dto.factory';
import { ConsoleLogger, Injectable } from '@nestjs/common';
import { CreateTransactionDtoValidationException } from '../exception/create-transaction-dto-validation.exception';
import { Command, CommandRunner } from 'nest-commander';

@Command({
  name: 'add',
  arguments: '<amount> <category>',
  description: 'Create transaction with amount and category.',
  options: { isDefault: true },
})
@Injectable()
export class TransactionRunner implements CommandRunner {
  private readonly logger = new ConsoleLogger('CLI:'.concat(TransactionRunner.name));

  constructor(private readonly transactionHandler: TransactionHandler) {}

  async run(
    passedParams: string[],
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    options?: Record<string, any>,
  ): Promise<void> {
    // TODO: add user id
    const [amount, category] = passedParams;
    this.logger.log(
      `Request to create transaction with amount: "${amount}", category: "${category}".`,
    );

    try {
      const dto = await CreateTransactionDtoFactory.create(amount, category);
      await this.transactionHandler.handleCreate(dto);

      this.logger.log(`Transaction successfully created.`);
    } catch (e) {
      let message = `Unhandled error: ${e}.`;

      if (e.constructor === CreateTransactionDtoValidationException) {
        message = e.toString(); // TODO: add some cli abstraction for error handling?
      }

      this.logger.error(message);
    }
  }
}
