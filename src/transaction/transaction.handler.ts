import { Inject, Injectable } from '@nestjs/common';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { TransactionRepositoryInterface } from './interface/transaction-repository.interface';
import { TransactionFactory } from './factory/transaction.factory';

@Injectable()
export class TransactionHandler {
  constructor(
    @Inject('PgTransactionRepository')
    private readonly transactionRepository: TransactionRepositoryInterface,
    private readonly transactionFactory: TransactionFactory,
  ) {}

  async handleCreate(dto: CreateTransactionDto): Promise<void> {
    const transaction = await this.transactionFactory.createFromDto(dto);

    await this.transactionRepository.create(transaction);
  }
}
