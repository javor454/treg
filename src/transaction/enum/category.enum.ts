export enum CategoryEnum {
  food = 'food',
  health = 'health',
  events = 'events',
  restaurants = 'restaurants',
  transport = 'transport',
  entertainment = 'entertainment',
}
