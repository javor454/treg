import { Injectable } from '@nestjs/common';
import { TransactionRepositoryInterface } from '../interface/transaction-repository.interface';
import { PgClient } from '../../pg/pg.client';
import { Transaction } from '../entity/transaction';

@Injectable()
export class TransactionRepository implements TransactionRepositoryInterface {
  constructor(private readonly client: PgClient) {}

  async create(transaction: Transaction): Promise<void> {
    await this.client.query(
      'INSERT INTO transaction.transaction(id, amount, category, timestamp) values ($1, $2, $3, $4);',
      [
        transaction.uuid,
        transaction.amount,
        transaction.category,
        transaction.timestamp.toISOString(),
      ],
    );
  }
}
