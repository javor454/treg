import {
  Body,
  Controller,
  HttpStatus,
  Logger,
  Post,
  Res,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Response } from 'express';
import { CreateTransactionDto } from '../dto/create-transaction.dto';
import { ApiBadRequestResponse, ApiTags } from '@nestjs/swagger';
import { TransactionHandler } from '../transaction.handler';

@Controller()
@ApiTags('Register transaction')
@UsePipes(new ValidationPipe())
export class TransactionController {
  private readonly logger = new Logger('HTTP:'.concat(TransactionController.name));

  constructor(private readonly handler: TransactionHandler) {}

  @Post('v1/transaction')
  @ApiBadRequestResponse({
    description:
      '<p>Field amount is not defined.</p><hr><br>' +
      '<p>Field amount is not a number.</p><hr><br>' +
      '<p>Field category is not defined.</p><hr><br>' +
      '<p>Field category is not a valid Category.</p>',
  })
  async create(
    @Body() createTransactionDto: CreateTransactionDto,
    @Res() response: Response,
  ): Promise<void> {
    this.logger.log(
      `Request to create transaction with amount: ${createTransactionDto.amount}, category: "${
        createTransactionDto.category
      }", timestamp: ${createTransactionDto.timestamp.toString()}.`,
    );
    await this.handler.handleCreate(createTransactionDto);

    this.logger.log('Transaction succesfully created.');
    response.sendStatus(HttpStatus.CREATED);
  }
}
