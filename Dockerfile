# Contains Node.js, npm
FROM node:16 as development

# Create dirs
RUN mkdir -p /home/node/treg/node_modules && chown -R node:node /home/node/treg

WORKDIR /home/node/treg

# Install app dependencies
COPY --chown=node:node ./package*.json ./
RUN npm install --verbose

# Copy app source
COPY --chown=node:node ./ ./
USER node

# Build the app and console
RUN npm run build
RUN npm run build-console

FROM node:16-alpine as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

RUN mkdir -p /home/node/treg/node_modules && chown -R node:node /home/node/treg

WORKDIR /home/node/treg

COPY --chown=node:node ./package*.json ./

USER node
RUN npm install --only=production

COPY --from=development --chown=node:node /home/node/treg/dist ./dist
COPY --from=development --chown=node:node /home/node/treg/migration ./migration

CMD ["node", "dist/main"]
