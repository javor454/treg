.PHONY: ps lint sh up upd down cli-help cli-add cli-mig test-cov test-e2e rm-cov dr-build-prod dr-ls dr-prune

# Utility
## List running containers
ps:
	docker-compose ps

## Run lint
lint:
	docker-compose exec treg npm run --rm lint

## Run lint
sh:
	docker-compose exec treg sh

# Dev environment
## Start dev env
up:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build -V

## Start dev env detached
upd:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build -dV

## Stop environment and remove containers
down:
	docker-compose down --remove-orphans


# Console
## List help for CLI interface (must be run on running container because of env dependencies)
cli-help:
	docker-compose exec treg npm run console:dev -- --help

## Create new transaction (use like: `make cli-tr-c AMOUNT="123.45" CATEGORY="food"`)
cli-add:
	docker-compose exec treg npm run console:dev -- add $$AMOUNT $$CATEGORY

## Run migrations
cli-mig:
	docker-compose exec treg npm run console:dev -- run

# Tests
## Run tests with coverage in running container
test-cov:
	docker-compose exec treg npm run test:cov

## Run e2e tests in running container
test-e2e:
	docker-compose exec treg npm run test:e2e

## Clear coverage dir
rm-cov:
	rm -rf coverage/


# Docker
## Build prod image
dr-build-prod:
	docker build . -t treg:latest

## List all images
dr-ls:
	 docker image ls

## Prune all docker related
dr-prune:
	docker kill > /dev/null 2>&1 || true && docker system prune -a -f && docker volume prune -f && docker network prune -f && docker system prune -a -f
