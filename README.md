# Treg

## Docker compose
Compose app
```bash
make up
```

Stop app
```bash
make down
```

## Rabbit
Rabbit admin
```bash
http://localhost:15672
```

## Elastic
Build docker image from root directory:
```bash

```

## Processes
### Bootstrap
- bootstrap cluster with dependencies
  - run migrations
  - create rabbit exchange and queue, bind
- bootstrap worker
  - start consuming queue

### Create transaction
- cli or http request
- validate input
- save to pg

### Create statement
- cli or http request
- validate input
- TODO get uuid of last transaction from pg
- TODO look for uuid and statement type in elastic
  - if exists return?
  - if not exists publish message with create statement job
- 

#### Worker
- consumes message with create statement job
- validate data
- create statistic
- save to elastic

## TODO
- kazda transakce obsahuje uuid
- kdyz pozadam o vytvoreni daily statementu, v handleru si vytahnu uuid posledni transakce
- pokud neni v elasticu dokument s `uuid=uuid transakce` a `statement type=daily`
    - producnu message do rabbita ze chci vygenerovat novy statement
    - vratim uuid transakce (nyni i statementu)
- pokud je
    - zparsuji do formatu a vratim?
