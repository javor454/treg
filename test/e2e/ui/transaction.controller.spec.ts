import { Test, TestingModule } from '@nestjs/testing';
import { ConsoleLogger, HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { TransactionModule } from '../../../src/transaction/transaction.module';

const CREATE_TRANSACTION_ENDPOINT_URL = '/v1/transaction';

describe('Transaction Controller', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [TransactionModule],
    })
      .setLogger(new ConsoleLogger())
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('POST /api/v1/transaction endpoint: should successfully create transaction', () =>
    request(app.getHttpServer())
      .post(CREATE_TRANSACTION_ENDPOINT_URL)
      .set({
        'Content-Type': 'application/json',
      })
      .send({
        amount: 123.45,
        category: 'food',
      })
      .expect(HttpStatus.CREATED));

  afterAll(async () => await app.close());
});
